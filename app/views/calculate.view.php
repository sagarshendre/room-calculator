<?php include('common/header.view.php'); 
      include('common/left.view.php');// print_r($_SESSION);exit; 
?>

        
<div class="col-10 middle" style="overflow: auto;">
    <!-- <span class="alert alert-success text-center float-right" id="success" style="float: right; margin: 0% 2%;;" ></span> -->

            <section class="content" style="margin-top: 3%;">
                <div class="container-fluid">
                                <span class="alert alert-success text-center" id="success" style="display: none;margin: 33.33%;"></span>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title" style="font-size: 1.15rem;">Room Calculator</h3>
                                </div>

                                <div class="card-body">
                                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6"></div>
                                            <div class="col-sm-12 col-md-6"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 ">
                                                    <!-- <form method="post" action="/Calculate/calculate_action"> -->
                                                    <form method="post" action="" id="calculateForm">

                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <label for="">No of adult</label> <span class="text text-danger">*</span> <?php if(!empty($errors['total_no_of_adults'])): ?> : <span class="text text-danger" id="erradults"><?= $errors['total_no_of_adults']; ?></span><?php endif;?><span class="text text-danger" id="erradults"></span>
                                                                <input type="text" class="form-control" id="total_no_of_adults" name="total_no_of_adults" placeholder="Please enter no of adult people in group" onkeypress="return isNumber(event)">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label for="">No of children</label> <?php if(!empty($errors['total_no_of_child'])): ?> : <span class="text text-danger"><?= $errors['total_no_of_child']; ?></span><?php endif;?><span class="text text-danger" id="errchild"></span>
                                                                <input type="text" class="form-control" id="total_no_of_child" name="total_no_of_child" placeholder="Please enter no of child people in group" onkeypress="return isNumber(event);" onkeyup ="addFields()">
                                                            </div>
                                                        </div></br></n>

                                                        <div class="row">
                                                            <div class="form-group col-md-12" >
                                                                <label for="">Age of child</label> <?php if(!empty($errors['child_age'])): ?> : <span class="text text-danger"><?= $errors['child_age']; ?></span><?php endif;?><span class="text text-danger" id="errage"></span>

                                                                <!-- <div class="col-md-1" id="container"> </div>  -->

                                                                <table >
                                                                    <tr id="newchild"></tr>
                                                                </table>
                                                            </div>
                                                        </div></br></n>

                                                            
                                                      
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary" id = "save" name = "save" >Calculate</button>
                                                        </div>

                                                    </form>
                                            </div>
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



            <section class="content" style="margin-top: 3%;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title" style="font-size: 1.15rem;">Result</h3>
                                </div>

                                <div class="card-body">
                                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6"></div>
                                            <div class="col-sm-12 col-md-6"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 ">

                                                    <table id="example2" class="table table-bordered table-hover dataTable dtr-inline" aria-describedby="example2_info">
                                                            <thead>
                                                                <tr >
                                                                    <th style="font-size: 0.9rem;">Adults Count</th>
                                                                    <th style="font-size: 0.9rem;">Child Count</th>
                                                                    <th style="font-size: 0.9rem;">Tripple Bed Rooms</th>
                                                                    <th style="font-size: 0.9rem;">Double Bed Rooms</th>
                                                                    <th style="font-size: 0.9rem;">Single Bed Rooms</th>
                                                                    <th style="font-size: 0.9rem;">Total Rooms</th>
                                                                    <th style="font-size: 0.9rem;">Total Cost</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr class="odd">
                                                                    <td class="dtr-control sorting_1" tabindex="0" ><span id="adults"></span></td>
                                                                    <td ><span id="child"></span></td>
                                                                    <td ><span id="threeroom"></span></td>
                                                                    <td ><span id="tworoom"></span></td>
                                                                    <td ><span id="oneroom"></span></td>
                                                                    <td ><span id="rooms"></span></td>
                                                                    <td ><span id="cost"></span></td>
                                                                </tr>
                                                            </tbody>
                                                    </table>

                                            </div>
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>




</div>

<?php
include('common/footer.view.php');
?>


<script type="text/javascript">
       
function  addFields(){

     var total_no_of_child  = $("#total_no_of_child").val();

     $("#newchild").empty();

        for (i=1;i<=total_no_of_child;i++){

            $("#newchild").append('<td id="row"><input type="text" name="child_age'+i+'" id="child_age'+i+'" placeholder="Enter age" class="form-control" onkeypress="return isNumber(event)" /></td>');

        }
   
    };

</script>

            
<script type="text/javascript">
    function isNumber(evt) {
        // alert('hii');
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
</script>

<script type="text/javascript">
    
        $("#save").click(function(e) {
          e.preventDefault();



            var total_no_of_adults = $("#total_no_of_adults").val(); 
            var total_no_of_child  = $("#total_no_of_child").val();



            var child_age = [];

            if(total_no_of_child==""){
                var total_no_of_child = 0;
            }


            for (i=1;i<=total_no_of_child;i++){
                // child_age        += ","+$("#child_age" + i).val();
                // child_age        += $("#child_age" + i).val();
                child_age.push($("#child_age" + i).val());

            var childlength = $("#child_age" + i).val().length;

            }

            if(total_no_of_child==""){
                var child_age = 0;
            }
            


            //validate----

           
                if(total_no_of_adults=="" || total_no_of_adults==0){
                    // $("#erradults").show().html("Required field.");

                    $('#erradults').fadeIn().html("Required field.");
                    setTimeout(function() {
                        $('#erradults').fadeOut("slow");
                    }, 2000 );

                }
                // else if(total_no_of_child=="")
                // {
                //      // $("#errchild").show().html("Required field.");

                //     $('#errchild').fadeIn().html("Required field.");
                //     setTimeout(function() {
                //         $('#errchild').fadeOut("slow");
                //     }, 2000 );

                // }
                else if(total_no_of_child!="" && (childlength == 0))
                {
                     // $("#errage").show().html("All field required.");

                    $('#errage').fadeIn().html("All field required.");
                    setTimeout(function() {
                        $('#errage').fadeOut("slow");
                    }, 2000 );

                }else
                {
                     var dataString = 'total_no_of_adults='+total_no_of_adults+'&total_no_of_child='+total_no_of_child+'&child_age='+child_age;


                  $.ajax({
                    type:'POST',
                    data:dataString,
                    url:'<?=ROOT?>/Calculate/calculate_action',
                    success:function(data) {
                      // alert(data);
                        var result = JSON.parse(data);

                        if (result!="") {
                            console.log(result.total_no_of_adults);
                            console.log(result.total_no_of_child);
                            console.log(result.total_no_of_rooms);
                            console.log(result.total_amount);


                            $('#total_no_of_adults').val('');
                            $('#total_no_of_child').val('');
                            $("#newchild").empty();

                            for (i=1;i<=total_no_of_child;i++){
                                $("#child_age" + i).val('');
                            }

                            $('#success').fadeIn().html("Calculation saved successfully.");

                            setTimeout(function() {
                                $('#success').fadeOut("slow");
                            }, 2000 );


                            $('#adults').html(result.total_no_of_adults);
                            $('#child').html(result.total_no_of_child);
                            $('#threeroom').html(result.three_bed_count);
                            $('#tworoom').html(result.two_bed_count);
                            $('#oneroom').html(result.one_bed_count);
                            $('#rooms').html(result.total_no_of_rooms);
                            $('#cost').html(result.total_amount);
                        }
                    }
                  });




              }

        });

</script>