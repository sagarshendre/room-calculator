<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
<link href="<?=ROOT?>/assets/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="<?=ROOT?>/assets/css/cover.css" rel="stylesheet">
  </head>

  <?php 

    if (!empty($_SESSION['USER'])) {
      $data['useremail'] = empty($_SESSION['USER']) ? 'User':$_SESSION['USER']->email;
      $data['username']  = empty($_SESSION['USER']) ? 'User':$_SESSION['USER']->user_name;

      $get_row      = "select * from users where email='".$data['useremail']."' limit 1";
      $get_user     = $this->get_row($get_row);

    }else{
      redirect('Login');
    }

    // print_r($_SESSION['USER']);exit;

  ?>
  
  <body class="d-flex h-100  text-white bg-dark">  <!-- // text-center-->
    
      <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">


            <header class="" style="margin-bottom:1%;">
            <!-- <header class="mb-auto"> -->
              <div>
                <!-- <h3 class="float-md-start mb-0">Welcome  $username </h3> -->
                <h3 class="float-md-start mb-0">Welcome <?= $data['username']; ?></h3>
                <nav class="nav nav-masthead justify-content-center float-md-end">
                  <a class="nav-link active" aria-current="page" href="#">Home</a>
                  <!-- <a class="nav-link" href="login">Login</a> -->
                  <a class="nav-link" href="<?=ROOT?>/Login/logout">Logout</a>
                </nav>
              </div>
            </header>

            
                    