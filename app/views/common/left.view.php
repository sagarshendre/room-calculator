<style>
.left{
    align-items: baseline;
    display: inline;
}
ul{
    list-style-type: none;
    padding-left: initial;
}

.wrapper .sidebar ul li a{
    display: block;
/*    padding: 0px 86px;*/
    border-bottom: 1px solid #10558d;
    color: rgb(241, 237, 237);
    font-size: 16px;
    position: relative;
    text-decoration: none;
/*    text-align: center;*/
}

.wrapper .sidebar ul li a .icon{
    color: #dee4ec;
    width: 30px;
    display: inline-block;
}



.wrapper .sidebar ul li a:hover,
.wrapper .sidebar ul li a.active{
    color: #0c7db1;

    background:white;
    border-right: 2px solid rgb(5, 68, 104);
}

.wrapper .sidebar ul li a:hover .icon,
.wrapper .sidebar ul li a.active .icon{
    color: #0c7db1;
}

.wrapper .sidebar ul li a:hover:before,
.wrapper .sidebar ul li a.active:before{
    display: block;
}
</style>

<?php
// function active($currect_page){
//   $url_array =  explode('/', $_SERVER['REQUEST_URI']) ;
//   $url = end($url_array);  
//   if($currect_page == $url){
//       echo 'active'; //class name in css 
//   } 
//}
?>


<?php
$page_url = $_SERVER['QUERY_STRING'];
$s = explode("=",$page_url);
//print $s[0];
$url = $s[1];
    // print_r($page);exit;
?>
<div class="row pannel-hight wrapper">
    <div class="col-2 left sidebar">
            <ul>
                <li>
                    <a href="<?=ROOT?>/Calculate_list" class="<?php if($url =='Calculate_list'){echo 'active';}?>">
                        <span class="icon"><i class="fas fa-desktop"></i></span>
                        <span class="item">Booking list</span>
                    </a>
                </li>
            <!--     <li>
                    <a href="<?php echo ROOT?>/Home" class="<?php if($url =='Home'){echo 'active';}?>">
                        <span class="icon"><i class="fas fa-home"></i></span>
                        <span class="item">Home</span>
                    </a>
                </li> -->
                <li>
                    <a href="<?=ROOT?>/Calculate" class="<?php if($url =='Calculate'){echo 'active';}?>">
                        <span class="icon"><i class="fas fa-desktop"></i></span>
                        <span class="item">Room Calculation</span>
                    </a>
                </li>
            </ul>
    </div>