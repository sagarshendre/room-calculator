<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Signin </title>


    <!-- Bootstrap core CSS -->
<link href="<?=ROOT?>/assets/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="<?=ROOT?>/assets/css/signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
    
      <main class="form-signin">
          <form method="post" action="<?=ROOT?>/Signup/signup_action">

            <?php if(!empty($errors)):?>
              <div class="alert alert-danger">
                <?= implode("<br>", $errors)?>
              </div>
            <?php endif;?>

            <img class="mb-4" src="<?=ROOT?>/assets/images/kratin-logo.png" alt="" width="72" height="57">
          
            <h1 class="h3 mb-3 fw-normal">Create account</h1>

            <div class="form-floating">
              <input name="email" type="text" class="form-control" id="floatingInput" placeholder="email@example.com" value="<?php if (isset($_POST['email'])) echo $_POST['email']; ?>">
              <label for="floatingInput">Email address</label>
            </div>
            <div class="form-floating">
              <input name="password" type="password" class="form-control" id="floatingPassword" placeholder="Password">
              <label for="floatingPassword">Password</label>
            </div>

          
            <button class="w-100 btn btn-lg btn-primary" type="submit">Create</button>
            <a href="<?=ROOT?>/login">Login</a>
            <p class="mt-5 mb-3 text-muted">&copy; 2022–2023</p>
          </form>
      </main>

  </body>
</html>
