<?php include('common/header.view.php'); 
      include('common/left.view.php');// print_r($_SESSION);exit; 
?>

        
<div class="col-10 middle">

            <section class="content" style="margin-top: 3%;">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title" style="font-size: 1.15rem;">Results</h3>
                                </div>

                                <div class="card-body">
                                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6"></div>
                                            <div class="col-sm-12 col-md-6"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 ">

                                                    <table id="example2" class="table table-bordered table-hover dataTable dtr-inline" aria-describedby="example2_info">
                                                            <thead>
                                                                <tr >
                                                                    <th style="font-size: 0.9rem;">Sr No</th>
                                                                    <th style="font-size: 0.9rem;">Adults Count</th>
                                                                    <th style="font-size: 0.9rem;">Child Count</th>
                                                                    <th style="font-size: 0.9rem;">Tripple Bed Rooms</th>
                                                                    <th style="font-size: 0.9rem;">Double Bed Rooms</th>
                                                                    <th style="font-size: 0.9rem;">Single Bed Rooms</th>
                                                                    <th style="font-size: 0.9rem;">Total Rooms</th>
                                                                    <th style="font-size: 0.9rem;">Total Cost</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                    <?php $i = 1;
                                                                        if (!empty($show_result)) { 
                                                                            foreach ($show_result as $value) {
                                                                                // print_r($value);exit;
                                                                    ?>
                                                                <tr class="odd">
                                                                    <td class="dtr-control sorting_1" tabindex="0" ><?= $i; ?></td>
                                                                    <td ><?= $value->total_no_of_adults; ?></td>
                                                                    <td ><?= $value->total_no_of_child; ?></td>
                                                                    <td ><?= $value->three_bed_count; ?></td>
                                                                    <td ><?= $value->two_bed_count; ?></td>
                                                                    <td ><?= $value->one_bed_count; ?></td>
                                                                    <td ><?= $value->total_no_of_rooms; ?></td>
                                                                    <td ><?= $value->total_amount; ?></td>

                                                                </tr>
                                                                <?php $i++; } } ?>
                                                            </tbody>
                                                    </table>

                                            </div>
                                        </div>
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>




</div>

<?php
include('common/footer.view.php');
?>

