<?php

class Visitor_log
{
    use Model;
    
    protected $table = 'visitor_group_logs';

    protected $allowedColumns = [

        'visitor_group_id',
		'age',
        'created',
	];
}