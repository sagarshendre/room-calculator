<?php

class Calculate_No
{
    use Model;
    
    protected $table = 'visitor_groups';

    protected $allowedColumns = [

        'user_id',
		'total_no_of_adults',
		'total_no_of_child',
        'total_no_of_rooms',
        'three_bed_count',
        'two_bed_count',
        'one_bed_count',
        'total_amount',
        // 'created',
	];

    public function validate($data)
    {
        $this->errors = [];

        if(empty($data['total_no_of_adults']))
        {
            $this->errors['total_no_of_adults'] = "No of adult field is required";
        }
        
        if(empty($data['total_no_of_child']))
        {
            $this->errors['total_no_of_child'] = "No of child field is required";
        }

        // for ($a=1; $a <= $data['total_no_of_child']; $a++) { 
        // // print_r($data['child_age'.$a]);exit;
        //     if (empty($data['child_age'.$a])) {
        //         $this->errors['child_age'] = 'Child age is required';  
        //     }
            
        // }
        
        if(empty($this->errors))
        {
            return true;
        }

        return false;
    }
}