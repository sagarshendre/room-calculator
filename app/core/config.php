<?php 

if($_SERVER['SERVER_NAME'] == 'localhost')
{
	/** database config **/
	define('DBNAME', 'task_room_calculator');
	define('DBHOST', 'localhost');
	define('DBUSER', 'root');
	define('DBPASS', '');
	define('DBDRIVER', '');
	
	define('ROOT', 'http://localhost/roomCalculator/room-calculator/public');

}else
{
	/** database config **/
	define('DBNAME', 'task_room_calculator');
	define('DBHOST', 'localhost');
	define('DBUSER', 'root');
	define('DBPASS', '');
	define('DBDRIVER', '');

	define('ROOT', 'http://10.10.103.102/roomCalculator/room-calculator/public');

}

define('APP_NAME', "My Webiste");
define('APP_DESC', "Testing website");

/** true means show errors **/
define('DEBUG', true);
