<?php 

/**
 * login class
 */
class Login
{
	use Controller;

	public function index()
	{
		// print_r($success);exit;
		$data = [];
		
		$data = array(

			'email' => isset($_POST['email'])?$_POST['email']:'',

		);

		$this->view('login',$data);
	}

	public function login_action()
	{
		
		$data = [];
		
		if($_SERVER['REQUEST_METHOD'] == "POST")
		{
			$user = new User;
			$arr['email'] = $_POST['email'];

			if($user->validate($_POST))
			{
			
				$row = $user->first($arr);
				// print_r($row->user_name);exit;
				
				if($row)
				{
					if($row->password === md5($_POST['password']))
					{
						$_SESSION['USER'] = $row;

						// print_r($_SESSION['USER']);exit;
						// redirect('home');
						redirect('Calculate_list');
					}else{
						$data['password_errors'] = "Password is incorrect";
						$this->view('login',$data);
						exit;

					}
				}
			}else{

				$data['errors'] = $user->errors;
			}

			// $user->errors['email'] = "Wrong email or password";

		}

		$this->view('login',$data);
	}

	public function logout()
	{

		// print_r($_SESSION['USER']);exit;

		if(!empty($_SESSION['USER']))
			session_start();
			session_destroy();
			unset($_SESSION['USER']);

		redirect('login');
	}

}
