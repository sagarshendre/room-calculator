<?php 

/**
 * signup class
 */
class Signup
{
	use Controller;

	public function index()
	{
		$data = [];
		
		$data = array(

			'email' => isset($_POST['email'])?$_POST['email']:'',

		);

		$this->view('signup',$data);
	}

	public function signup_action()
	{
		$data = [];
		
		if($_SERVER['REQUEST_METHOD'] == "POST")
		{
			$user = new User;
			if($user->validate_signup($_POST))
			{

				// print_r($_POST['email']);exit;
				
					$data = array(
						'email'   		=> $_POST['email'],
						'password'   	=> md5($_POST['password']),
					);

					$user->insert($data);
					// redirect('login');

					$data_success['success'] = "Signup successfull";

				$this->view('login',$data_success);
					exit;

			}

			$data['errors'] = $user->errors;			
		}


		$this->view('signup',$data);
	}

}
