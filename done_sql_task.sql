
1-)    SELECT 
      `sfi`.`id` as `id`,`student_id`,`stu`.`first_name` as `student_name`,
      SUM(`amount`) as `amount`, 
      SUM(`discountAmount`) as `discountAmount`, 
      SUM(`paid_amount`) as `paid_amount`, 
      SUM(`writeoff_amount`) as `writeoff_amount`, 
      SUM(`amount` - (`paid_amount` + `discountAmount` + `writeoff_amount`)) AS `Unpaid_amount` 
      FROM `student_fee_line_items` as `sfi` 
      LEFT JOIN `students` as `stu` 
      ON `sfi`.`student_id` = `stu`.`id` 
      WHERE `sfi`.`status`="Unpaid" 
      Group By `sfi`.`student_id`;




2-)  WithSection

SELECT `stu`.`id` as `student_id`, `ssta2`.`stand_name` as `joining_standard`,`bs2`.`sectionName` as `joining_section`, `ssta1`.`stand_name` as `current_standard`, `bs1`.`sectionName` as `current_section` 
FROM `students` as `stu` 
INNER JOIN `student_standards` as `ssta1` ON `stu`.`student_std_id` = `ssta1`.`id` 
INNER JOIN `student_standards` as `ssta2` ON `stu`.`joining_standard` = `ssta2`.`id` 
INNER JOIN `academic_data` as `ad1` ON `stu`.`academic_year_id` = `ad1`.`academic_session_id` 
INNER JOIN `batch_section` as `bs1` ON `ad1`.`batch_id` = `bs1`.`id` 
INNER JOIN `academic_data` as `ad2` ON `stu`.`joining_academic_year_id` = `ad2`.`academic_session_id` 
INNER JOIN `batch_section` as `bs2` ON `ad2`.`batch_id` = `bs2`.`id` 
WHERE `stu`.`id`= 595
GROUP BY `stu`.`id`
ORDER BY `stu`.`id` ASC;




3-) SELECT `b2`.`id`,`b2`.`isbn`,GROUP_CONCAT( `b2`.`accession_number` ) as `accession_number`,count( `b2`.`isbn`)

           FROM `book` as `b2`
           WHERE `b2`.`status` != 'Scrap' and `b2`.`isbn` != 'Null'  
           GROUP BY `isbn`  
           HAVING count(`b2`.`isbn`) > 1;




4-) 
SELECT `dis`.`name` as `discount_type`,`sflid`.`sfli_id`,`sflid`.`discount_amount`,SUM(`sflid`.`discount_amount`)
FROM `student_fee_line_item_discount` as `sflid` 
INNER JOIN `discounts` as `dis` ON `sflid`.`discount_head_id` = `dis`.`id`
WHERE `sflid`.`sfli_id` IN
(
  SELECT `sfli`.`id`
  FROM `student_fee_line_items` as `sfli`
  INNER JOIN `student_discount` as `sd` ON `sfli`.`student_id` = `sd`.`student_id`
  WHERE `sfli`.`discountAmount` > '0' AND `sfli`.`student_id` = '595' AND `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
)
GROUP BY `dis`.`id`;


5-) SELECT `ssl`.`student_id`,`stu`.`academic_year_id`,  `ssl`.`academic_session_id`, `ssl`.`student_status_master_id` 
FROM `students` as `stu`
INNER JOIN `student_status_logs` as `ssl` ON  `stu`.`id` = `ssl`.`student_id`
INNER JOIN `student_status_master` as `ssm` ON  `ssl`.`student_status_master_id` = `ssm`.`id`
WHERE `stu`.`is_active`= 0 
      AND (`stu`.`academic_year_id` = 9 AND `ssl`.`academic_session_id` = 9) 
      AND (`ssl`.`student_status_master_id` = 1 OR `ssl`.`student_status_master_id` = 2)
ORDER BY `ssl`.`id` DESC
LIMIT 1



6-) SELECT `sb`.`sibbling_id`, `stu`.`id` as `student_id` , CONCAT_WS(' ', `g`.`first_name`,`g`.`last_name`) as `guardian_name` , `g`.`mobile_phone` as `guardian_mobile_phone` 
FROM `students` as `stu` 
INNER JOIN `sibbling_details` as `sb` 
ON `stu`.`id` = `sb`.`student_id` 
INNER JOIN `student_guardian` as `sg` 
ON `stu`.`id` = `sg`.`sid` 
INNER JOIN `guardians` as `g` 
ON `sg`.`gid` = `g`.`id` 
WHERE `stu`.`is_deleted` = 1
GROUP BY `g`.`id` 
ORDER BY `g`.`last_name` ASC;



7-) 
SELECT `sflid`.`student_id`,SUM(`sflid`.`discount_amount`),SUM(`sfli`.`discountAmount`), (SUM(`sflid`.`discount_amount`)-SUM(`sfli`.`discountAmount`)) as `discount_remaind`
FROM `student_fee_line_item_discount` as `sflid` 
INNER JOIN `student_fee_line_items` as `sfli` ON `sflid`.`sfli_id` = `sfli`.`id`
GROUP BY `sfli`.`student_id`
HAVING (SUM(`sflid`.`discount_amount`)-SUM(`sfli`.`discountAmount`))!=0;



