1)   SELECT `id`,`student_id`,`amount`,`discountAmount`,`paid_amount`,`writeoff_amount`,(`amount` - `discountAmount`) AS `Unpaid_amount` FROM `student_fee_line_items` WHERE status="Unpaid";

1) SELECT `id`,`student_id`,`amount`,`discountAmount`,`paid_amount`,`writeoff_amount`,(`amount` - (`discountAmount` + `paid_amount` + `writeoff_amount`)) AS `Unpaid_amount` FROM `student_fee_line_items` WHERE status="Unpaid";

1) SELECT `sfi`.`id` as `student_items_id`,`student_id`,`stu`.`first_name` as `student_name`,`amount`,`discountAmount`,`paid_amount`,`writeoff_amount`,(`amount` - (`discountAmount` + `paid_amount` + `writeoff_amount`)) AS `Unpaid_amount` FROM `student_fee_line_items` as `sfi` LEFT JOIN `students` as `stu` ON `sfi`.`student_id` = `stu`.`id` WHERE `sfi`.`status`="Unpaid";

1-)    SELECT 
      `sfi`.`id` as `id`,`student_id`,`stu`.`first_name` as `student_name`,
      SUM(`amount`) as `amount`, 
      SUM(`discountAmount`) as `discountAmount`, 
      SUM(`paid_amount`) as `paid_amount`, 
      SUM(`writeoff_amount`) as `writeoff_amount`, 
      SUM(`amount` - (`paid_amount` + `discountAmount` + `writeoff_amount`)) AS `Unpaid_amount` 
      FROM `student_fee_line_items` as `sfi` 
      LEFT JOIN `students` as `stu` 
      ON `sfi`.`student_id` = `stu`.`id` 
      WHERE `sfi`.`status`="Unpaid" 
      Group By `sfi`.`student_id`;




2) SELECT `stu`.`id`, `ssta2`.`stand_name` as `joining_standard`, `ssta1`.`stand_name` as `current_standard` 
FROM `students` as `stu` 
INNER JOIN `student_standards` as `ssta1` ON `stu`.`student_std_id` = `ssta1`.`id` 
INNER JOIN `student_standards` as `ssta2` ON `stu`.`joining_standard` = `ssta2`.`id` 
ORDER BY `stu`.`id` ASC


2) SELECT `stu`.`id` as `student_id`, `ssta2`.`stand_name` as `joining_standard`, `ssta1`.`stand_name` as `current_standard` 
FROM `students` as `stu` 
INNER JOIN `student_standards` as `ssta1` ON `stu`.`student_std_id` = `ssta1`.`id` 
INNER JOIN `student_standards` as `ssta2` ON `stu`.`joining_standard` = `ssta2`.`id` 
ORDER BY `stu`.`id` ASC

2) SELECT `stu`.`id` as `student_id`, `ssta2`.`stand_name` as `joining_standard`, `ssta1`.`stand_name` as `current_standard` 
FROM `students` as `stu` 
INNER JOIN `student_standards` as `ssta1` ON `stu`.`student_std_id` = `ssta1`.`id` 
INNER JOIN `student_standards` as `ssta2` ON `stu`.`joining_standard` = `ssta2`.`id` 
ORDER BY `stu`.`id` ASC


2-)  Without Section

SELECT `stu`.`id` as `student_id`, `ssta2`.`stand_name` as `joining_standard`, `ssta1`.`stand_name` as `current_standard` 
FROM `students` as `stu` 
INNER JOIN `student_standards` as `ssta1` ON `stu`.`student_std_id` = `ssta1`.`id` 
INNER JOIN `student_standards` as `ssta2` ON `stu`.`joining_standard` = `ssta2`.`id` 
ORDER BY `stu`.`id` ASC

2-)  SELECT `stu`.`id` as `student_id`, `ssta2`.`stand_name` as `joining_standard`, `ssta1`.`stand_name` as `current_standard`, `bs1`.`sectionName` as `current_section` 
FROM `students` as `stu` 
INNER JOIN `student_standards` as `ssta1` ON `stu`.`student_std_id` = `ssta1`.`id` 
INNER JOIN `student_standards` as `ssta2` ON `stu`.`joining_standard` = `ssta2`.`id` 
INNER JOIN `academic_data` as `ad1` ON `stu`.`academic_year_id` = `ad1`.`academic_session_id` 
INNER JOIN `batch_section` as `bs1` ON `ad1`.`batch_id` = `bs1`.`id` 
GROUP BY `stu`.`id`
ORDER BY `stu`.`id` ASC


2-)  WithSection

SELECT `stu`.`id` as `student_id`, `ssta2`.`stand_name` as `joining_standard`,`bs2`.`sectionName` as `joining_section`, `ssta1`.`stand_name` as `current_standard`, `bs1`.`sectionName` as `current_section` 
FROM `students` as `stu` 
INNER JOIN `student_standards` as `ssta1` ON `stu`.`student_std_id` = `ssta1`.`id` 
INNER JOIN `student_standards` as `ssta2` ON `stu`.`joining_standard` = `ssta2`.`id` 
INNER JOIN `academic_data` as `ad1` ON `stu`.`academic_year_id` = `ad1`.`academic_session_id` 
INNER JOIN `academic_data` as `ad2` ON `stu`.`joining_academic_year_id` = `ad2`.`academic_session_id` 
INNER JOIN `batch_section` as `bs1` ON `ad1`.`batch_id` = `bs1`.`id` 
INNER JOIN `batch_section` as `bs2` ON `ad2`.`batch_id` = `bs2`.`id` 
ORDER BY `stu`.`id` ASC




3-) SELECT `b2`.`id`,`b2`.`isbn`,GROUP_CONCAT( `b2`.`accession_number` ) as `accession_number`,count( `b2`.`isbn`)

           FROM `book` as `b2`
           WHERE `b2`.`status` != 'Scrap' and `b2`.`isbn` != 'Null'  
           GROUP BY `isbn`  
           HAVING count(`b2`.`isbn`) > 1;

3)  SELECT `accession_number`, `isbn`, `status`, COUNT(*) 
    FROM `book` 
    WHERE `status` != 'Scrap'
    GROUP BY `isbn` 
    HAVING count(*) > 1;

3)  SELECT `accession_number`, `isbn`, `status`, COUNT(*) 
    FROM `book` 
    WHERE `status` != 'Scrap' 
    GROUP BY `isbn` 
    HAVING count(`isbn`) > 1 
    ORDER BY `id` DESC;

3) Select
STRING_AGG( ISNULL(`accession_number`, ' '), ',') As `accession_number`
FROM `book` as `b1`
JOIN(
SELECT `accession_number`, `isbn`, `status`, COUNT(*) 
FROM `book` 
WHERE `status` != 'Scrap' 
GROUP BY `isbn` 
HAVING count(`isbn`) > 1
) as `b2`
ON `b1`.`isbn` = `b2`.`isbn`





Select `b1`.`isbn`,
STRING_AGG( `b1`.`accession_number`, ',') As `accession_number`
FROM `book` as `b1`
JOIN(
SELECT `accession_number`, `isbn`, `status`, COUNT(*) 
FROM `book` 
WHERE `status` != 'Scrap' 
GROUP BY `isbn` 
HAVING count(`isbn`) > 1
) as `b2`
ON `b1`.`isbn` = `b2`.`isbn`
GROUP BY `b1`.`isbn` ;


SELECT `accession_number`, `isbn`, `status`, COUNT(*) 
`accession_number` = 
STUFF((SELECT ', ' + `accession_number`
           FROM `book` as `b2`
           WHERE `b2`.`id` =  `b1`.`id`
           FOR XML PATH('')), 1, 2, '')

FROM `book` as `b1`
WHERE `status` != 'Scrap' 
GROUP BY `isbn` 
HAVING count(`isbn`) > 1






REFER


SELECT a.*
FROM users a
JOIN (SELECT username, email, COUNT(*)
FROM users 
GROUP BY username, email
HAVING count(*) > 1 ) b
ON a.username = b.username
AND a.email = b.email
ORDER BY a.email



4) SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),FROM `students` as `stu`
INNER JOIN `student_discount` as `sd` ON `stu`.`id` = `sd`.`student_id`           
INNER JOIN `student_fee_line_item_discount` as `sflid` ON `sd`.`id` = `sflid`.`discount_head_id`
WHERE `sd`.`student_id` = 595;


4) SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`sfli`.`discountAmount`
FROM `students` as `stu`
INNER JOIN `student_discount` as `sd` ON `stu`.`id` = `sd`.`student_id`           
INNER JOIN `student_fee_line_items` as `sfli` ON `sd`.`student_id` = `sfli`.`student_id`
WHERE `sd`.`student_id` = 595;


4) SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`sfli`.`discountAmount`
FROM `students` as `stu`
INNER JOIN `student_discount` as `sd` ON `stu`.`id` = `sd`.`student_id`           
INNER JOIN `student_fee_line_items` as `sfli` ON `stu`.`id` = `sfli`.`student_id`
WHERE `sd`.`student_id` = '595' AND `sd`.`end_date` >= '2023-01-17' AND `sd`.`start_date` <= '2023-01-17';


4) SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`sfli`.`discountAmount`
FROM `students` as `stu`
INNER JOIN `student_discount` as `sd` ON `stu`.`id` = `sd`.`student_id`           
INNER JOIN `student_fee_line_items` as `sfli` ON `stu`.`id` = `sfli`.`student_id`
WHERE `sd`.`student_id` = '595' AND `sd`.`start_date` <= (select date('now')) AND `sd`.`end_date` >= (select date('now'));


4-) INITIAL

SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`sfli`.`discountAmount`
FROM `students` as `stu`
INNER JOIN `student_discount` as `sd` ON `stu`.`id` = `sd`.`student_id`           
INNER JOIN `student_fee_line_items` as `sfli` ON `stu`.`id` = `sfli`.`student_id`
WHERE `sd`.`student_id` = '595' AND `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
GROUP BY `sfli`.`id`;


4-) INITIAL

SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`sfli`.`discountAmount`
FROM `students` as `stu`
INNER JOIN `student_discount` as `sd` ON `stu`.`id` = `sd`.`student_id`           
INNER JOIN `student_fee_line_items` as `sfli` ON `stu`.`id` = `sfli`.`student_id`
INNER JOIN `student_fee_line_item_discount` as `sflid` ON `stu`.`id` = `sflid`.`student_id`
WHERE `sd`.`student_id` = '595' AND `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
GROUP BY `sfli`.`id`;


4-) INITIAL

SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`sfli`.`discountAmount`,`sflid`.`sfli_id`,`sflid`.`discount_date`,`sflid`.`discount_amount`,`sd`.`end_date`
FROM `students` as `stu`
INNER JOIN `student_discount` as `sd` ON `stu`.`id` = `sd`.`student_id`           
INNER JOIN `student_fee_line_items` as `sfli` ON `stu`.`id` = `sfli`.`student_id`
INNER JOIN `student_fee_line_item_discount` as `sflid` ON `sfli`.`id` = `sflid`.`sfli_id`
WHERE `sd`.`student_id` = '595' AND `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
GROUP BY `sfli`.`id`;


4-) INITIAL

SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`sfli`.`discountAmount`,`sflid`.`sfli_id`,`sflid`.`discount_date`,`sflid`.`discount_amount`,`sd`.`end_date`
FROM `students` as `stu`
INNER JOIN `student_discount` as `sd` ON `stu`.`id` = `sd`.`student_id`           
INNER JOIN `student_fee_line_items` as `sfli` ON `stu`.`id` = `sfli`.`student_id`
INNER JOIN `student_fee_line_item_discount` as `sflid` ON `sfli`.`id` = `sflid`.`sfli_id`
WHERE `sd`.`student_id` = '595' AND `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
GROUP BY `sflid`.`id`;


4-) INITIAL

SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`sfli`.`discountAmount`,`sflid`.`sfli_id`,`sflid`.`discount_date`,`sflid`.`discount_amount`,`sd`.`end_date`
FROM `students` as `stu`
INNER JOIN `student_discount` as `sd` ON `stu`.`id` = `sd`.`student_id`           
INNER JOIN `student_fee_line_items` as `sfli` ON `stu`.`id` = `sfli`.`student_id`
INNER JOIN `student_fee_line_item_discount` as `sflid` ON `sfli`.`id` = `sflid`.`sfli_id`
WHERE `sd`.`student_id` = '595' AND `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
GROUP BY `sflid`.`id`;


4-) INITIAL

SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`sfli`.`discountAmount`,`sflid`.`sfli_id`,`sflid`.`discount_date`,`sflid`.`discount_amount`,`sd`.`end_date`
FROM `students` as `stu`
INNER JOIN `student_discount` as `sd` ON `stu`.`id` = `sd`.`student_id`           
INNER JOIN `student_fee_line_items` as `sfli` ON `stu`.`id` = `sfli`.`student_id`
INNER JOIN `student_fee_line_item_discount` as `sflid` ON `sfli`.`id` = `sflid`.`sfli_id`
WHERE `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
GROUP BY `sflid`.`id`  


4-) INITIAL

SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`sfli`.`discountAmount`,`sflid`.`sfli_id`,`sflid`.`discount_date`,GROUP_CONCAT( `sflid`.`discount_amount` ) as `discount_amount`
FROM `students` as `stu`
INNER JOIN `student_discount` as `sd` ON `stu`.`id` = `sd`.`student_id`           
INNER JOIN `student_fee_line_items` as `sfli` ON `stu`.`id` = `sfli`.`student_id`
INNER JOIN `student_fee_line_item_discount` as `sflid` ON `sfli`.`id` = `sflid`.`sfli_id`
WHERE `sd`.`student_id` = '595' AND `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
GROUP BY `sflid`.`id`
ORDER BY `sflid`.`discount_date` DESC; 


4-) INITIAL

SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`sfli`.`discountAmount`,`sflid`.`sfli_id`,`sflid`.`discount_date`,GROUP_CONCAT( `sflid`.`discount_amount` ) as `discount_amount`
FROM `students` as `stu`
INNER JOIN `student_discount` as `sd` ON `stu`.`id` = `sd`.`student_id`           
INNER JOIN `student_fee_line_items` as `sfli` ON `stu`.`id` = `sfli`.`student_id`
INNER JOIN `student_fee_line_item_discount` as `sflid` ON `sfli`.`id` = `sflid`.`sfli_id`
WHERE `sd`.`student_id` = '595' AND `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
GROUP BY `sflid`.`sfli_id` AND `sfli`.`discountAmount`
ORDER BY `sflid`.`discount_date` DESC;

-------------

4) SELECT `dis`.`name` as `discount_type`,`sfli`.`discountAmount`,`sflid`.`sfli_id`,`sflid`.`discount_date`,`sflid`.`discount_amount` as `discount_amount`
FROM `student_fee_line_items` as `sfli`
INNER JOIN `student_discount` as `sd` ON `sfli`.`student_id` = `sd`.`student_id`
INNER JOIN `student_fee_line_item_discount` as `sflid` ON `sfli`.`id` = `sflid`.`sfli_id`
INNER JOIN `discounts` as `dis` ON `sflid`.`discount_head_id` = `dis`.`id`
WHERE `sfli`.`student_id` = '595' AND `sfli`.`discountAmount` > '0' AND  `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
GROUP BY `sflid`.`sfli_id`;

4) SELECT `dis`.`name` as `discount_type`,`sfli`.`discountAmount`,`sflid`.`sfli_id`,`sflid`.`discount_date`,`sflid`.`discount_amount` as `discount_amount`
FROM `student_fee_line_items` as `sfli`
INNER JOIN `student_discount` as `sd` ON `sfli`.`student_id` = `sd`.`student_id`
INNER JOIN `student_fee_line_item_discount` as `sflid` ON `sfli`.`id` = `sflid`.`sfli_id`
INNER JOIN `discounts` as `dis` ON `sflid`.`discount_head_id` = `dis`.`id`
WHERE `sfli`.`student_id` = '595' AND `sfli`.`discountAmount` > '0' AND  `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
GROUP BY `sflid`.`id`;


4) SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`dis`.`name` as `discount_type`,`sfli`.`discountAmount`,`sflid`.`sfli_id`,`sflid`.`discount_date`,SUM(`sflid`.`discount_amount`) as `discount_amount`
FROM `student_fee_line_items` as `sfli`
INNER JOIN `students` as `stu` ON `sfli`.`student_id` = `stu`.`id`           
INNER JOIN `student_discount` as `sd` ON `sfli`.`student_id` = `sd`.`student_id`
INNER JOIN `student_fee_line_item_discount` as `sflid` ON `sfli`.`id` = `sflid`.`sfli_id`
INNER JOIN `discounts` as `dis` ON `sflid`.`discount_head_id` = `dis`.`id`
WHERE `sfli`.`student_id` = '595' AND `sfli`.`discountAmount` > '0' AND  `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
GROUP BY `sflid`.`id`;


STEP 1

SELECT `sfli`.*
FROM `student_fee_line_items` as `sfli`
WHERE `sfli`.`discountAmount` > '0' AND `sfli`.`student_id` = '595' AND `sfli`.`academic_year_id` = '9';


STEP 2

SELECT `dis`.`name` as `discount_type`,`sflid`.`sfli_id`,`sflid`.`discount_amount`,SUM(`sflid`.`discount_amount`)
FROM `student_fee_line_item_discount` as `sflid` 
INNER JOIN `discounts` as `dis` ON `sflid`.`discount_head_id` = `dis`.`id`
WHERE `sflid`.`sfli_id` IN
(
  SELECT `sfli`.`id`
  FROM `student_fee_line_items` as `sfli`
  INNER JOIN `student_discount` as `sd` ON `sfli`.`student_id` = `sd`.`student_id`
  WHERE `sfli`.`discountAmount` > '0' AND `sfli`.`student_id` = '595' AND `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
)
GROUP BY `dis`.`id`;


STEP 3

SELECT CONCAT_WS(' ', `stu`.`first_name`,`stu`.`last_name`),`dis`.`name`,`sflid1`.`sfli_id`,`sflid1`.`discount_amount`,SUM(`sflid1`.`discount_amount`)
FROM `student_fee_line_item_discount` as `sflid1` 
INNER JOIN `students` as `stu` ON `sflid1`.`student_id` = `stu`.`id`
INNER JOIN `discounts` as `dis` ON `sflid1`.`discount_head_id` = `dis`.`id`
WHERE `sflid1`.`id` IN
(
  SELECT `sflid2`.`id`
  FROM `student_fee_line_item_discount` as `sflid2`
  WHERE `sflid2`.`sfli_id` IN
    (
      SELECT `sfli`.`id`
      FROM `student_fee_line_items` as `sfli`
      INNER JOIN `student_discount` as `sd` ON `sfli`.`student_id` = `sd`.`student_id`
      WHERE `sfli`.`discountAmount` > '0' AND `sfli`.`student_id` = '595' AND `sfli`.`academic_year_id` = '9' AND `sd`.`end_date` >= (select date('now'))
    )
)
GROUP BY `dis`.`id`;



5) SELECT `ssl`.`student_id`,`stu`.`academic_year_id`,  `ssl`.`academic_session_id`, `ssl`.`student_status_master_id` 
FROM `students` as `stu`
INNER JOIN `student_status_logs` as `ssl` ON  `stu`.`id` = `ssl`.`student_id`
INNER JOIN `student_status_master` as `ssm` ON  `ssl`.`student_status_master_id` = `ssm`.`id`
WHERE `stu`.`is_active`= 0 AND (`stu`.`academic_year_id` = 9 AND `ssl`.`academic_session_id` = 9)


5) SELECT `ssl`.`student_id`,`stu`.`academic_year_id`,  `ssl`.`academic_session_id`, `ssl`.`student_status_master_id` 
FROM `students` as `stu`
INNER JOIN `student_status_logs` as `ssl` ON  `stu`.`id` = `ssl`.`student_id`
INNER JOIN `student_status_master` as `ssm` ON  `ssl`.`student_status_master_id` = `ssm`.`id`
WHERE `stu`.`is_active`= 0 
      AND (`stu`.`academic_year_id` = 9 AND `ssl`.`academic_session_id` = 9) 
      AND (`ssl`.`student_status_master_id` = 1 OR `ssl`.`student_status_master_id` = 2)
ORDER BY `ssl`.`id` DESC
GROUP BY `ssl`.`student_id`


5) SELECT `ssl`.`student_id`,`stu`.`academic_year_id`,  `ssl`.`academic_session_id`, `ssl`.`student_status_master_id` 
FROM `students` as `stu`
INNER JOIN `student_status_logs` as `ssl` ON  `stu`.`id` = `ssl`.`student_id`
INNER JOIN `student_status_master` as `ssm` ON  `ssl`.`student_status_master_id` = `ssm`.`id`
WHERE `stu`.`is_active`= 0 
      AND (`stu`.`academic_year_id` = 9 AND `ssl`.`academic_session_id` = 9) 
      AND (`ssl`.`student_status_master_id` = 1 OR `ssl`.`student_status_master_id` = 2)
ORDER BY `ssl`.`id` DESC
LIMIT 1



6) SELECT `sb`.`sibbling_id` 
FROM `students` as `stu` 
INNER JOIN `sibbling_details` as `sb` 
ON `stu`.`id` = `sb`.`student_id` 
WHERE `stu`.`is_deleted` = 1;

6) SELECT `sb`.`sibbling_id`, `stu`.`id` as `student_id` 
FROM `students` as `stu` 
INNER JOIN `sibbling_details` as `sb` 
ON `stu`.`id` = `sb`.`student_id` 
WHERE `stu`.`is_deleted` = 1;

6) SELECT `sb`.`sibbling_id`, `stu`.`id` as `student_id` 
FROM `students` as `stu` 
INNER JOIN `sibbling_details` as `sb` 
ON `stu`.`id` = `sb`.`student_id` 
INNER JOIN `student_guardian` as `sg` 
ON `stu`.`id` = `sg`.`sid` 
WHERE `stu`.`is_deleted` = 1;

6) SELECT `sb`.`sibbling_id`, `stu`.`id` as `student_id` , `g`.`first_name` as `guardian_first_name` ,`g`.`last_name` as `guardian_last_name` , `g`.`mobile_phone` as `guardian_mobile_phone` 
FROM `students` as `stu` 
INNER JOIN `sibbling_details` as `sb` 
ON `stu`.`id` = `sb`.`student_id` 
INNER JOIN `student_guardian` as `sg` 
ON `stu`.`id` = `sg`.`sid` 
INNER JOIN `guardians` as `g` 
ON `sg`.`gid` = `g`.`id` 
WHERE `stu`.`is_deleted` = 1
GROUP BY `sb`.`student_id`
;


6) SELECT `sb`.`sibbling_id`, `stu`.`id` as `student_id` , `g`.`first_name` as `guardian_first_name` ,`g`.`last_name` as `guardian_last_name` , `g`.`mobile_phone` as `guardian_mobile_phone` 
FROM `students` as `stu` 
INNER JOIN `sibbling_details` as `sb` 
ON `stu`.`id` = `sb`.`student_id` 
INNER JOIN `student_guardian` as `sg` 
ON `stu`.`id` = `sg`.`sid` 
INNER JOIN `guardians` as `g` 
ON `sg`.`gid` = `g`.`id` 
WHERE `stu`.`is_deleted` = 1
GROUP BY `sb`.`student_id` AND `sb`.`sibbling_id`
ORDER BY `g`.`last_name` ASC;

6) SELECT `sb`.`sibbling_id`, `stu`.`id` as `student_id` , `g`.`first_name` as `guardian_first_name` ,`g`.`last_name` as `guardian_last_name` , `g`.`mobile_phone` as `guardian_mobile_phone` 
FROM `students` as `stu` 
INNER JOIN `sibbling_details` as `sb` 
ON `stu`.`id` = `sb`.`student_id` 
INNER JOIN `student_guardian` as `sg` 
ON `stu`.`id` = `sg`.`sid` 
INNER JOIN `guardians` as `g` 
ON `sg`.`gid` = `g`.`id` 
WHERE `stu`.`is_deleted` = 1
GROUP BY `g`.`id` 
ORDER BY `g`.`last_name` ASC;



6-) SELECT `sb`.`sibbling_id`, `stu`.`id` as `student_id` , CONCAT_WS(' ', `g`.`first_name`,`g`.`last_name`) as `guardian_name` , `g`.`mobile_phone` as `guardian_mobile_phone` 
FROM `students` as `stu` 
INNER JOIN `sibbling_details` as `sb` 
ON `stu`.`id` = `sb`.`student_id` 
INNER JOIN `student_guardian` as `sg` 
ON `stu`.`id` = `sg`.`sid` 
INNER JOIN `guardians` as `g` 
ON `sg`.`gid` = `g`.`id` 
WHERE `stu`.`is_deleted` = 1
GROUP BY `g`.`id` 
ORDER BY `g`.`last_name` ASC;



