-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 09, 2023 at 07:33 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `task_room_calculator`
--

-- --------------------------------------------------------

--
-- Table structure for table `room_types`
--

CREATE TABLE `room_types` (
  `id` int(11) NOT NULL,
  `room_type` enum('Single bed','Double bed','Triple bed','Extra bed') NOT NULL DEFAULT 'Single bed',
  `rate_per_night` int(11) NOT NULL,
  `no_of_bed` int(11) NOT NULL,
  `created_by_user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `room_types`
--

INSERT INTO `room_types` (`id`, `room_type`, `rate_per_night`, `no_of_bed`, `created_by_user_id`, `created`, `modified`) VALUES
(1, 'Triple bed', 2750, 3, 1, '2023-01-02 15:03:59', '2023-01-02 15:03:59'),
(2, 'Double bed', 2000, 2, 1, '2023-01-02 15:03:59', '2023-01-02 15:03:59'),
(3, 'Single bed', 1500, 1, 1, '2023-01-02 15:04:57', '2023-01-02 15:04:57'),
(4, 'Extra bed', 500, 0, 1, '2023-01-02 15:05:13', '2023-01-02 15:05:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role` enum('Admin','Visitor') DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `user_name`, `email`, `password`, `created`, `modified`) VALUES
(1, NULL, NULL, 'email@email.com', '827ccb0eea8a706c4c34a16891f84e7b', '2023-01-06 17:43:26', '2023-01-06 17:43:26'),
(2, NULL, NULL, 'sagar@email.com', '827ccb0eea8a706c4c34a16891f84e7b', '2023-01-09 10:20:01', '2023-01-09 10:20:01');

-- --------------------------------------------------------

--
-- Table structure for table `visitor_groups`
--

CREATE TABLE `visitor_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `total_no_of_adults` int(3) DEFAULT NULL,
  `total_no_of_child` int(3) DEFAULT NULL,
  `no_of_extra_bed_required` int(3) DEFAULT NULL,
  `total_no_of_rooms` int(3) DEFAULT NULL,
  `three_bed_count` int(3) DEFAULT NULL,
  `two_bed_count` int(3) DEFAULT NULL,
  `one_bed_count` int(3) DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visitor_group_logs`
--

CREATE TABLE `visitor_group_logs` (
  `id` int(11) NOT NULL,
  `visitor_group_id` int(11) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT current_timestamp(),
  `modified` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `room_types`
--
ALTER TABLE `room_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitor_groups`
--
ALTER TABLE `visitor_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitor_group_logs`
--
ALTER TABLE `visitor_group_logs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `room_types`
--
ALTER TABLE `room_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `visitor_groups`
--
ALTER TABLE `visitor_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visitor_group_logs`
--
ALTER TABLE `visitor_group_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
